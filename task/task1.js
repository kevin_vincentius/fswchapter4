 /*test
    1. Do NOT change any of the existing code.
    2. You are NOT allowed to type any numbers.
    3. You should NOT redeclare the variable a and b.
    4. When the code is run, it should output: 
    a is 8 
    b is 3
    */

function test(){
   
    let a ="3";
    let b ="8";
 
    [a,b] = [b,a];

    console.log("a is "+a);
    console.log("b is "+b);
}
test();
