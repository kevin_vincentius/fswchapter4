//task

// #1 Gunakan metode untuk membalikan isi array (pertama ke terakhir, terakhir ke pertama)
console.log("soal 1");
console.log("-----------")
let fruits = ["Apple", "Banana", "Papaya", "Grape", "Cherry", "Peach"];

function moveElements(fruits, fromIndex, toIndex) {
  // Remove the element from the 'fromIndex' position
  const element = fruits.splice(fromIndex, 1)[0];

  // Insert the element at the 'toIndex' position
  fruits.splice(toIndex, 0, element);

  // Return the modified array
  return fruits;
}
console.log("before: ",fruits)

moveElements(fruits,0,fruits.length-1);
moveElements(fruits,fruits.length-2,0);
console.log("after: ",fruits)
console.log("----------")

/* 
Output: 
Peach
Cherry
Grape
Papaya
Banana
Apple
*/

// #2 Gunakan metode untuk menyisipkan isi array 

let month = [
  "January",
  "February",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

function addMonths (month){
  month.splice(2,0,"March","April","May","June");
  return month;
}
console.log("soal 2");
console.log("---------------")
console.log("before: ", month);
addMonths(month);
console.log("after: ",month)
console.log("---------------")



/* 
Output: 
 ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
*/

// #3
const message = "Sampaikan pada Sabrina belajar Javascript sangat menyenangkan";

 kata1 = message.slice(23,30);
 kata2 = message.slice(31, 42);
 kata3 = message.slice(49,64);

console.log("soal 3");
console.log("---------------")
const extractedmessage = kata1 + " " +kata2 + " " +kata3;
console.log(extractedmessage)
console.log("---------------")

/* 
Output: belajar Javascript menyenangkan
*/

// #4
console.log("soal 4");
console.log("---------------")
function capitalizeFirstLetter(extractedmessage) {
  return extractedmessage.charAt(0).toUpperCase() + extractedmessage.slice(1);
}

let capitalizedSentence = capitalizeFirstLetter(extractedmessage)
console.log(capitalizedSentence)
console.log("---------------")


/* 
Output: Belajar Javascript menyenangkan
*/

/* #5 Buat function untuk menghitung BMI (Body Mass Index) */

// mass = 78;
// height = 1.69;
console.log("soal 5");
console.log("---------------")
function hitungBMI(mass, height){
  BMI = mass/ (height**2);
  return BMI;
}
hitungBMI(78,1.69);
console.log(BMI);



/* - BMI = mass / height ** 2 = mass / (height * height) (mass in kg and height in meter)
- Steven weights 78 kg and is 1.69 m tall. Bill weights 92 kg and is 1.95 
m tall
*/


/* #6 Menghitung BMI dengan if else statement
- John weights 95 kg and is 1.88 m tall. Nash weights 85 kg and is 1.76 
m tall

Output example:
John's BMI (28.3) is higher than Nash's (23.5)
*/

console.log("soal 6");
console.log("---------------")
let johnBMI = hitungBMI(95, 1.88);
let nashBMI = hitungBMI(85, 1.76);

if(johnBMI > nashBMI){
  console.log(`John's BMI (${johnBMI}) is higher than Nash's (${nashBMI})`)
}else{
  console.log(`Nash's (${nashBMI}) is higher than John's BMI (${johnBMI})`)
}
console.log("---------------")


//  #7 Looping

let data = [10, 20, 30, 40, 50];
let total;

/* You code here (you are allowed to reassigned the variable) 
Maybe you can write 3 lines or more
Use for, do while, while for, or forEach
*/

function sumArrayElements (arr){
  let sum = 0;
  for (let i =0; i<arr.length;i++){
    sum += arr[i];
  }
  return sum;
}
total = sumArrayElements(data);

console.log(`Jumlah total = ${total}`);

/* 
Jumlah total = 150
*/
